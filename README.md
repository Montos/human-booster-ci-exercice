diazdjapzidojazpd
Ahmed Chohra 
# Exercices Git, Gitlab et CI

1. Créer un fork de ce projet 
    - [ ] Vous connecter sur Gitlab (ou créer un compte)
    - [ ] Créer un fork par groupe : cliquer sur le bouton "fork" en haut de [l'écran du projet](https://gitlab.com/drakona-formation/human-booster-ci-exercice),
    - [ ] Si vous travaillez à deux (ou plus), invitez-vous sur le projet
    - [ ] Puis inviter @Dreeckan (Rémi Jarjat ou remi@drakona.fr) dans le projet, avec les droits d'écriture et de merge (maintainer)


2. Cloner ce projet sur vos machines (idéalement, en ligne de commande ;) ) et lancer la commande `composer install` (pour installer Symfony et toutes les dépendances du projet)


3. Ajouter les auteurs du projet
    - [ ] Créer une branche et une merge request sur Gitlab (sur votre projet, et pas sur le projet parent)
    - [ ] Récupérer cette branche en local
    - [ ] Ajouter le fichier et son contenu en local
        - [ ] Chaque personne du groupe devra inscrire son nom (un exemple de format que vous n'êtes pas obligés de suivre : `Rémi Jarjat <remi@drakona.fr>`) et faire un commit
    - [ ] Mettre à jour la merge request et la relire à deux (vérifier que le contenu du fichier correspond aux attentes et que les 2 commits sont bien présents)
    - [ ] Merge dans votre projet une fois les corrections validées


4. Lancer les tests unitaires (`./bin/phpunit` dans la ligne de commande sous Linux ou `php bin/phpunit` sous windows)
    - [ ] Créer une branche et une merge request sur Gitlab
    - [ ] Récupérer cette branche en local
    - [ ] Corriger les tests pour qu'ils ne renvoient plus une erreur et soient cohérents
    - [ ] Mettre à jour la MR (Merge Request) et la faire relire (ou la relire à deux)
    - [ ] Merge dans votre projet une fois les corrections validées


5. Lancer phpStan (`vendor/bin/phpstan analyse` dans la ligne de commande sous Linux) (si vous n'arrivez pas à lancer phpStan, l'erreur est quelque part dans le dossier `src/`)
    - [ ] Créer une branche et une merge request sur Gitlab
    - [ ] Récupérer cette branche en local
    - [ ] Corriger le code pour ne plus avoir d'erreur phpStan
    - [ ] Mettre à jour la MR (Merge Request) et la faire relire (ou la relire à deux)
    - [ ] Merge dans votre projet une fois les corrections validées


6. Créer un fichier de CI gitlab ([Une aide sur Elevent Labs](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/) ou [un exemple de fichier gitlab](https://gitlab.com/snippets/1994308))
    - [ ] Créer une branche et une merge request sur Gitlab
    - [ ] Récupérer cette branche en local
    - [ ] Créer le fichier de CI (.gitlab-ci.yml)
        - [ ] Ajouter 3 tâches (jobs) : 
            - [ ] Installation des vendors de Symfony et mise en cache de ce dossier
            - [ ] Lancement des tests unitaires
            - [ ] Lancement de phpStan et rendre cette tâche optionnelle (si elle renvoie une erreur, elle ne doit pas arrêter le pipeline)
        - [ ] Répartir ces tâches dans au moins 2 étapes (stages)
        - [ ] Utiliser une image Docker pour lancer le pipeline (pour ceux qui préfèrent ne pas chercher, vous pouvez utiliser celle-ci : `drakona/php:7.2-ci`)
    - [ ] Mettre à jour la MR et la faire relire (ou la relire à deux)
    - [ ] Merge dans votre projet une fois les corrections validées

7. Créer une merge request entre le master de votre projet et le master du [projet d'origine](https://gitlab.com/drakona-formation/human-booster-ci-exercice)
